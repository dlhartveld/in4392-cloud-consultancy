package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class JobTest {

	private static final User user = new User(123, "user");
	private static final String url = "url://0";

	private Job job;

	@Before
	public void setUp() {
		job = new Job(user, url, null);
	}

	@Test
	public void testThatNewJobIsNotRunning() throws Exception {
		assertThat(job.isRunning(), is(false));
	}

	@Test
	public void testThatNewJobIsNotCompleted() throws Exception {
		assertThat(job.isCompleted(), is(false));
	}

	@Test
	public void testThatStartedJobIsRunning() throws Exception {
		job.setStartTime(123);
		assertThat(job.isRunning(), is(true));
	}

	@Test
	public void testThatStartedJobIsNotCompleted() throws Exception {
		job.setStartTime(123);
		assertThat(job.isCompleted(), is(false));
	}

	@Test
	public void testThatStoppedJobIsNotRunning() throws Exception {
		job.setStartTime(110);
		job.setCompletionTime(123);
		assertThat(job.isRunning(), is(false));
	}

	@Test
	public void testThatStoppedJobIsCompleted() throws Exception {
		job.setStartTime(110);
		job.setCompletionTime(123);
		assertThat(job.isCompleted(), is(true));
	}

}
