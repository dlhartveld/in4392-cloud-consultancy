package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Cluster;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.DummyCluster;

import org.junit.Test;

public class JobTrackerFactoryTest {

	@Test
	public void testThatSerializationAndDeserializationWork() {
		Cluster cluster = new DummyCluster();
		JobTrackerFactory factory = new JobTrackerFactory(cluster, "target/deserializaton-test.data");

		JobTracker jobTracker = new JobTracker(cluster, null);
		jobTracker.submit(new Job(new User(0, "User 0"), "url://0", null));
		jobTracker.submit(new Job(new User(1, "User 1"), "url://1", null));
		factory.serialize(jobTracker);

		JobTracker deserialized = factory.deserialize();

		TrackerStatus status = deserialized.inspect();
		assertThat(status.getQueueSize(), is(2));

	}

}
