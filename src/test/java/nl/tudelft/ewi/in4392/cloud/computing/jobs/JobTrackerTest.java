package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Cluster;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.DummyCluster;

import org.jgroups.Address;
import org.jgroups.stack.IpAddress;
import org.junit.Before;
import org.junit.Test;

public class JobTrackerTest {

	private JobTracker tracker;

	@Before
	public void setUp() {
		Cluster cluster = new DummyCluster();
		tracker = new JobTracker(cluster, null);
	}

	@Test
	public void testThatJobsAreTracked() throws Exception {

		User user = new User(110, "user");

		String gitUrl0 = "url://0";
		String gitUrl1 = "url://1";

		Job job0 = new Job(user, gitUrl0, null);
		Job job1 = new Job(user, gitUrl1, null);

		tracker.submit(job0);
		tracker.submit(job1);
		
		Address address = new IpAddress();

		Job executingJob0 = tracker.takeForExecutionOn(address);
		assertThat(executingJob0, is(sameInstance(job0)));
		assertThat(executingJob0.isRunning(), is(true));
		assertThat(executingJob0.isCompleted(), is(false));

		TrackerStatus whileRunning = tracker.inspect();
		assertThat(whileRunning.getNumJobsInProgress(), is(1));

		tracker.onCompletionOf(address);

		assertThat(executingJob0.isRunning(), is(false));
		assertThat(executingJob0.isCompleted(), is(true));

		TrackerStatus afterCompletion = tracker.inspect();
		assertThat(afterCompletion.getNumJobsInProgress(), is(0));
		assertThat(afterCompletion.getQueueSize(), is(1));
	}

}
