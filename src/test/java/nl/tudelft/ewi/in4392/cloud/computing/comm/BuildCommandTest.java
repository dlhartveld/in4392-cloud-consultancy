package nl.tudelft.ewi.in4392.cloud.computing.comm;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

public class BuildCommandTest {

	private static final Logger LOG = LoggerFactory.getLogger(BuildCommandTest.class);

	private static final String URL = "git://github.com/dlhartveld/mini-project.git";

	private File workingDirectory;

	@Before
	public void setUp() throws Exception {
		workingDirectory = Files.createTempDir();
		LOG.debug("Created working directory: {}", workingDirectory.getAbsolutePath());
	}

	@After
	public void tearDown() throws Exception {
		LOG.debug("Deleting working directory: {}", workingDirectory.getAbsolutePath());
		try {
			FileUtils.deleteDirectory(workingDirectory);
		} catch (IOException e) {
			LOG.warn("Failed to delete working directory: {} - delete it manually.",
					workingDirectory.getAbsolutePath());
		}
	}

	@Test
	public void test() throws Exception {

		LOG.debug("Creating BuildCommand with URL: {}", URL);
		BuildCommand cmd = new BuildCommand(URL);

		LOG.debug("Executing BuildCommand...");
		cmd.execute();

		LOG.debug("Test finished.");

	}

}
