package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;
import java.util.List;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

public class VmIdentify extends Command<String> {

	private final long vmId;

	public VmIdentify(long vmId) {
		this.vmId = vmId;
	}

	@Override
	String execute(InputOutput io) throws VmIdentityException {
		try {
			io.record(true);
			io.writeLine("onevm show " + vmId);
			List<String> results = io.readResponse();
			io.record(false);

			for (String result : results) {
				String trimmed = result.trim();
				if (trimmed.startsWith("IP=")) {
					return trimmed.substring(4, trimmed.length() - 2);
				}
			}

			throw new VmIdentityException("Could not create VM: " + results);
		} catch (IOException e) {
			throw new VmIdentityException(e);
		}
	}

	public class VmIdentityException extends RuntimeException {

		private static final long serialVersionUID = 7322342492133900397L;

		public VmIdentityException(Throwable e) {
			super(e);
		}

		public VmIdentityException(String message) {
			super(message);
		}

	}

}
