package nl.tudelft.ewi.in4392.cloud.computing.cluster;

import org.jgroups.Address;
import org.jgroups.View;

import com.google.inject.ImplementedBy;

@ImplementedBy(ClusterIOImpl.class)
public interface ClusterIO {

	Address ensureConnected() throws Exception;

	void disconnect();

	View getView();

	Address getAddress();

	Address getMaster();

	String getHostName();
	
	void send(Address to, Object o);

}
