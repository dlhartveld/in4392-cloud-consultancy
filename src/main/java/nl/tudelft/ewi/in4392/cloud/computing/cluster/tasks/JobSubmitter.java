package nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks;

import nl.tudelft.ewi.in4392.cloud.computing.jobs.Job;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.JobTracker;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JobSubmitter implements Runnable {
	
	private static final Logger LOG = LoggerFactory.getLogger(JobSubmitter.class);
	
	private JobTracker jobTracker;

	public void setJobTracker(JobTracker tracker) {
		this.jobTracker = tracker;
	}

	@Override
	public void run() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				Job job = new Job(new User(0, "Michael de Jong"), "git://github.com/michaeldejong/Java-Gitolite-Manager.git", null);
				LOG.info("Submitting job: " + job.getGitUrl());
				jobTracker.submit(job);
			}
		});
		
		t.start();
	}
	
}
