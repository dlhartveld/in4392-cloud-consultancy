package nl.tudelft.ewi.in4392.cloud.computing.cluster;

import javax.inject.Inject;

import nl.tudelft.ewi.in4392.cloud.computing.comm.BuildCommand;
import nl.tudelft.ewi.in4392.cloud.computing.comm.BuildException;
import nl.tudelft.ewi.in4392.cloud.computing.comm.BuildResult;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Cluster;

import org.jgroups.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

public class SlaveRole implements Role, Runnable {

	private static final Logger LOG = LoggerFactory.getLogger(SlaveRole.class);

	// Suicide after 5 minutes of inactivity. 
	private static final long TIMEOUT = 60000 * 5;
	
	private final Cluster cluster;
	private final ClusterIO io;
	
	private Thread thread;
	private State state = null;
	private long timeout = Long.MAX_VALUE;

	@Inject
	public SlaveRole(Cluster cluster, ClusterIO io) {
		this.io = io;
		this.cluster = cluster;
	}

	@Override
	public void onStart() {
		LOG.info("Assuming SLAVE role...");
		LOG.info("Host in MASTER role: " + io.getMaster());
		
		switchState(State.IDLE);
		
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void onMessage(Address src, Container content) {
		LOG.info("Received message: {} - from source: {}", content, src);

		Preconditions.checkNotNull(src, "src");
		Preconditions.checkNotNull(content, "content");

		if (content.isA(BuildCommand.class)) {
			BuildCommand command = content.getContent();
			LOG.info("Received build command: {}", command);
			BuildResult result = startNewBuild(command);
			LOG.info("Sending result (" + result.isSuccess() + ") to master.");
			io.send(src, result);
		}
		else {
			LOG.warn("Ignored message with content: " + content.getClazz());
		}
	}

	@Override
	public void onTerminate() {
		LOG.info("Releasing SLAVE role...");
		stopShutdownTimer();
	}

	@Override
	public void onElectionStart() {
		stopShutdownTimer();
	}

	@Override
	public void onElectionEnd(Address master) {
		startShutdownTimer();
	}

	private void switchState(State newState) {
		State previous = state;
		state = newState;
		if (previous != State.IDLE && newState == State.IDLE) {
			startShutdownTimer();
		}
		else if (previous != State.BUSY && newState == State.BUSY) {
			stopShutdownTimer();
		}
	}

	private void stopShutdownTimer() {
		timeout = Long.MAX_VALUE;
	}

	private void startShutdownTimer() {
		timeout = System.currentTimeMillis() + TIMEOUT;
	}
	
	private BuildResult startNewBuild(BuildCommand command) {
		BuildResult result = null;
		
		switchState(State.BUSY);
		try {
			command.execute();
			result = new BuildResult(command.getGitUrl(), true);
		}
		catch (BuildException e) {
			result = new BuildResult(command.getGitUrl(), false);
		}
		switchState(State.IDLE);
		
		return result;
	}

	private enum State {
		BUSY, IDLE;
	}

	@Override
	public void run() {
		LOG.info("Running...");

		while (timeout > System.currentTimeMillis()) {
			try {
				Thread.sleep(10000);
			} 
			catch (InterruptedException e) {
				LOG.warn("Sleep interrupted", e);
			}
		}
		
		LOG.info("Suiciding because of inactivity...");
		cluster.shutdownMachine();
	}

}
