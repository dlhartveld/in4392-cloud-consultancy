package nl.tudelft.ewi.in4392.cloud.computing.ssh.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class InputOutput {

	private final Input input;
	private final Output output;

	public InputOutput() {
		input = new Input();
		output = new Output();
	}

	public InputStream getInputStream() {
		return input.getInputStream();
	}

	public OutputStream getOutputStream() {
		return output.getOutputStream();
	}

	public List<String> readResponse() throws IOException {
		return output.readResponse();
	}

	public void writeLine(String line) throws IOException {
		input.writeLine(line);
	}

	public void close() throws IOException {
		input.close();
		output.close();
	}

	public void record(boolean active) throws IOException {
		output.record(active);
	}

}
