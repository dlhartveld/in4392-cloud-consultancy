package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.util.List;

import javax.inject.Singleton;

import com.google.common.collect.Lists;

@Singleton
public class DummyCluster implements Cluster {

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#listMachines()
	 */
	@Override
	public List<Vm> listMachines() {
		return Lists.newArrayList();
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#bootMachine()
	 */
	@Override
	public long bootMachine() {
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#launchSystem(java.lang.String)
	 */
	@Override
	public boolean launchSystem(String ipAddress) {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#shutdownMachine(long)
	 */
	@Override
	public void shutdownMachine(long vmId) {
		// Do nothing.
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#deleteMachine(long)
	 */
	@Override
	public void deleteMachine(long vmId) {
		// Do nothing
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#disconnect()
	 */
	@Override
	public void disconnect() {
		// Do nothing.
	}

	@Override
	public String identify(long id) {
		return null;
	}

	@Override
	public boolean copyFileToNode(String ipAddress, String file) {
		return false;
	}
	
	@Override
	public boolean copyFileFromNode(String ipAddress, String file) {
		return false;
	}

	@Override
	public void disableFirewall(String ipAddress) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getIpAddress() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void shutdownMachine() {
		// TODO Auto-generated method stub
		
	}

}
