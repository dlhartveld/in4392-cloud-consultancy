package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import javax.inject.Singleton;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Cluster;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jgroups.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@SuppressWarnings("serial")
@Singleton
public class JobTracker implements Serializable {

	private static final Logger LOG = LoggerFactory.getLogger(JobTracker.class);

	private final Queue<Job> queue;
	private final Map<Address, Job> inProgress;

	private final Map<User, List<Job>> jobArchive;
	private final String location;
	private transient Cluster cluster;

	public JobTracker(Cluster cluster, String location) {
		this.cluster = cluster;
		this.location = location;
		queue = Lists.newLinkedList();
		inProgress = Maps.newHashMap();
		jobArchive = Maps.newHashMap();
	}

	public synchronized void submit(Job job) {
		LOG.debug("Job submitted: {}", job);

		checkNotNull(job, "job");

		addToQueue(job);
		addToArchive(job);
		
		save();
	}

	public synchronized Job takeForExecutionOn(Address node) {
		if (queue.peek() == null) {
			return null;
		}

		Job job = queue.poll();
		job.setStartTime(System.currentTimeMillis());
		inProgress.put(node, job);
		
		save();

		return job;
	}

	public synchronized void onCompletionOf(Address node) {
		checkNotNull(node, "node");
		checkArgument(inProgress.containsKey(node), "Node is not registered as being in progress");

		Job job = inProgress.get(node);
		job.setCompletionTime(System.currentTimeMillis());
		inProgress.remove(node);
		
		save();
	}

	public synchronized TrackerStatus inspect() {
		return new TrackerStatus(queue, inProgress, jobArchive);
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		builder.append("queue", queue);
		return builder.toString();
	}

	private void addToQueue(Job job) {
		queue.offer(job);
	}

	private void addToArchive(Job job) {
		if (!jobArchive.containsKey(job.getUser())) {
			jobArchive.put(job.getUser(), new ArrayList<Job>());
		}
		jobArchive.get(job.getUser()).add(job);
	}

	public void setCluster(Cluster cluster) {
		Preconditions.checkNotNull(cluster);
		this.cluster = cluster;
	}
	
	private void save() {
		if (location != null) {
			new JobTrackerFactory(cluster, location).serialize(this);
		}
	}

}
