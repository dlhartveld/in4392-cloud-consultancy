package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

public class Exit extends Command<Boolean> {

	@Override
	Boolean execute(InputOutput io) throws ExitException {
		try {
			io.record(true);
			io.writeLine("exit");
			io.record(false);
			return true;
		} catch (IOException e) {
			throw new ExitException(e);
		}
	}

	public class ExitException extends RuntimeException {

		private static final long serialVersionUID = 8904500504725606513L;

		public ExitException(Throwable e) {
			super(e);
		}

		public ExitException(String message) {
			super(message);
		}

	}

}
