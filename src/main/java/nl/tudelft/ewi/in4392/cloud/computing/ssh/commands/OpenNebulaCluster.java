package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;

@Singleton
public class OpenNebulaCluster implements Cluster {
	
	private static final Logger LOG = LoggerFactory.getLogger(OpenNebulaCluster.class);

	private final Executor executor;
	private final Properties properties;
	private final String ipAddress;

	@Inject
	public OpenNebulaCluster(@Named("ipAddress") String ipAddress, Executor executor, Properties properties) {
		this.ipAddress = ipAddress;
		this.executor = executor;
		this.properties = properties;
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#listMachines()
	 */
	@Override
	public List<Vm> listMachines() {
		return executeCommand(new VmList(), "Could not get listing of Virtual Machines.");
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#bootMachine()
	 */
	@Override
	public long bootMachine() {
		String imageLocation = properties.getProperty("image");
		return executeCommand(new VmCreate(imageLocation), "Could not boot Virtual Machine using image: " + imageLocation);
	}
	
	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#launchSystem(java.lang.String)
	 */
	@Override
	public boolean launchSystem(String ipAddress) {
		return executeCommand(new Launch(ipAddress, "node.jar"), "Could not launch: node.jar");
	}
	
	@Override
	public boolean copyFileToNode(String ipAddress, String file) {
		return executeCommand(new CopyFileTo(ipAddress, file), "Could not copy file: " + file);
	}
	
	@Override
	public boolean copyFileFromNode(String ipAddress, String file) {
		return executeCommand(new CopyFileFrom(ipAddress, file), "Could not copy file: " + file);
	}
	
	@Override
	public String identify(long vmId) {
		return executeCommand(new VmIdentify(vmId), "Could not identify vm: " + vmId);
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#shutdownMachine(long)
	 */
	@Override
	public void shutdownMachine(long vmId) {
		executeCommand(new VmShutdown(vmId), "Could not shutdown Virtual Machine: " + vmId);
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#deleteMachine(long)
	 */
	@Override
	public void deleteMachine(long vmId) {
		executeCommand(new VmDelete(vmId), "Could not delete Virtual Machine: " + vmId);
	}

	@Override
	public void disableFirewall(String ipAddress) {
		executeCommand(new DisableFirewall(ipAddress), "Could not disable firewall at node: " + ipAddress);
	}

	@Override
	public void shutdownMachine() {
		Long vmId = getVmId();
		if (vmId >= 0) {
			shutdownMachine(vmId);
		}
		else {
			LOG.error("Cannot terminate machine, because I couldn't figure out the vmId");
		}
	}
	
	private Long getVmId() {
		List<Vm> machines = listMachines();
		for (Vm machine : machines) {
			String machineIp = identify(machine.getId());
			if (machineIp != null && machineIp.equals(ipAddress)) {
				return machine.getId();
			}
		}
		return null;
	}

	@Override
	public String getIpAddress() {
		return ipAddress;
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.ICluster#disconnect()
	 */
	@Override
	public void disconnect() {
		try {
			if (executor.isConnected()) {
				executeCommand(new Exit(), "Could not exit SSH terminal");
			}
			executor.disconnect();
		} catch (IOException e) {
			throw new ClusterException("Could not disconnect from SSH terminal", e);
		}
	}

	private <T> T executeCommand(Command<T> command, String errorMessage) {
		try {
			return executor.execute(command);
		} catch (JSchException e) {
			LOG.error(e.getMessage(), e);
			throw new ClusterException(errorMessage, e);
		}
	}

	public class ClusterException extends RuntimeException {

		private static final long serialVersionUID = -1038898587414397043L;

		public ClusterException(String message, Throwable cause) {
			super(message, cause);
		}

	}

}
