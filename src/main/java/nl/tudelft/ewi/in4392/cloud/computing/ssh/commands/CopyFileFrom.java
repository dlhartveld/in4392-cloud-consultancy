package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;
import java.util.List;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CopyFileFrom extends Command<Boolean> {
	
	private static final Logger LOG = LoggerFactory.getLogger(CopyFileFrom.class);
	
	private final String ipAddress;
	private final String fileName;
	
	public CopyFileFrom(String ipAddress, String fileName) {
		this.ipAddress = ipAddress;
		this.fileName = fileName;
	}

	@Override
	Boolean execute(InputOutput io) {
		try {
			boolean retry = true;
			while (retry) {
				LOG.info("Copying file: " + fileName + " from: " + ipAddress);
				io.record(true);
				io.writeLine("scp root@" + ipAddress + ":" + fileName + " " + fileName);
				List<String> responses = io.readResponse();
				io.record(false);
				if (!responses.contains("lost connection")) {
					retry = false;
				}
			}
			return true;
		}
		catch (IOException e) {
			return false;
		}
	}

}
