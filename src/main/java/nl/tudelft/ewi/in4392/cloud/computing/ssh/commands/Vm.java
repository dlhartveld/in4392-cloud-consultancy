package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

public class Vm {

	private final long id;
	private final String user;
	private final String groups;
	private final String name;
	private final State state;
	private final double cpu;
	private final long mem;
	private final String host;
	private final int uptime;

	public Vm(long id, String user, String groups, String name, State state, double cpu, long mem, String host, int uptime) {
		this.id = id;
		this.user = user;
		this.groups = groups;
		this.name = name;
		this.state = state;
		this.cpu = cpu;
		this.mem = mem;
		this.host = host;
		this.uptime = uptime;
	}

	public long getId() {
		return id;
	}

	public String getUser() {
		return user;
	}

	public String getGroups() {
		return groups;
	}

	public String getName() {
		return name;
	}

	public State getState() {
		return state;
	}

	public double getCpu() {
		return cpu;
	}

	public long getMem() {
		return mem;
	}

	public String getHost() {
		return host;
	}

	public int getUptime() {
		return uptime;
	}

	@Override
	public String toString() {
		return "VM[" + id + ", " + state + "\thost: " + host + ", cpu: " + cpu + ", mem: " + mem + "K, uptime: " + uptime + "s]";
	}

	public enum State {
		ONLINE("runn"),
		OFFLINE("shut"),
		BOOTING("pend"),
		UNKNOWN("");

		private final String name;

		private State(String name) {
			this.name = name;
		}

		public static State named(String name) {
			for (State state : values()) {
				if (state.name.equals(name)) {
					return state;
				}
			}
			return UNKNOWN;
		}
	}

}