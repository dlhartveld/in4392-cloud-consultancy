package nl.tudelft.ewi.in4392.cloud.computing.ssh.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.BlockingQueue;

import com.google.common.collect.Queues;

class Input {

	private final InputStream in;
	private final BlockingQueue<Integer> queue;

	public Input() {
		queue = Queues.newArrayBlockingQueue(1000);

		in = new InputStream() {
			@Override
			public int read() throws IOException {
				try {
					return queue.take();
				}
				catch (InterruptedException e) {
					throw new IOException(e);
				}
			}
		};
	}

	public InputStream getInputStream() {
		return in;
	}

	public synchronized void writeLine(String line) throws IOException {
		try {
			for (int i = 0; i < line.length(); i++) {
				queue.put((int) line.charAt(i));
			}
			queue.put((int) '\r');
			queue.put(-1);
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

	public synchronized void close() throws IOException {
		try {
			queue.put(-1);
			in.close();
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

}
