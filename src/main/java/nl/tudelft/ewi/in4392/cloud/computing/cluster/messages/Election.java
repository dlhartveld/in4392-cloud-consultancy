package nl.tudelft.ewi.in4392.cloud.computing.cluster.messages;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.jgroups.Address;

@Data
@AllArgsConstructor
public class Election implements Serializable {

	private static final long serialVersionUID = -6150629108970426995L;

	private final Address sender;
	private final long viewTime;
	private Address proposedMaster;
	
}
