package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class JobMgmtGuiceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(String.class).annotatedWith(Names.named("jobtracker.data")).toInstance("jobs.json");
	}

}
