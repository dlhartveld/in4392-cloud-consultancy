package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

abstract class Command<R> {

	abstract R execute(InputOutput io);

}
