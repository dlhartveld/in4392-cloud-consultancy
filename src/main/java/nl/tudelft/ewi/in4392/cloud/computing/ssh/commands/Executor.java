package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;
import java.util.Properties;

import javax.inject.Inject;
import javax.inject.Singleton;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

@Singleton
class Executor {

	private static final Logger LOG = LoggerFactory.getLogger(Executor.class);
	
	private final String host;
	private final String user;
	private final String pass;
	private final Object lock = new Object();

	private Session session = null;
	private Channel channel = null;
	private InputOutput io = null;

	@Inject
	public Executor(Properties properties) {
		this(properties.getProperty("host"), properties.getProperty("user"), properties.getProperty("pass"));
	}

	public Executor(String host, String user, String pass) {
		this.host = host;
		this.user = user;
		this.pass = pass;
	}

	public <T> T execute(Command<T> command) throws JSchException {
		synchronized (lock) {
			ensureSshConnection();
			return command.execute(io);
		}
	}

	public void disconnect() throws IOException {
		synchronized (lock) {
			channel.disconnect();
			session.disconnect();
			io.close();
		}
	}

	public boolean isConnected() {
		synchronized (lock) {
			return session.isConnected() && channel.isConnected();
		}
	}

	private void ensureSshConnection() throws JSchException {
		if (session == null || !session.isConnected()) {
			JSch jsch = new JSch();
			jsch.setKnownHosts("~/.ssh/known_hosts");

			session = jsch.getSession(user, host, 22);
			if (pass != null) {
				session.setPassword(pass);
			}

			UserInfo ui = new UserInfo() {
				@Override
				public boolean promptYesNo(String message) {
					return true;
				}

				@Override
				public String getPassphrase() {
					return null;
				}

				@Override
				public String getPassword() {
					return null;
				}

				@Override
				public boolean promptPassphrase(String arg0) {
					return false;
				}

				@Override
				public boolean promptPassword(String arg0) {
					return false;
				}

				@Override
				public void showMessage(String arg0) {
					LOG.info(arg0);
				}
			};

			session.setUserInfo(ui);
			session.connect(60000);
		}

		if (channel == null || !channel.isConnected()) {
			channel = session.openChannel("shell");
			((ChannelShell) channel).setAgentForwarding(true);

			io = new InputOutput();
			channel.setInputStream(io.getInputStream());
			channel.setOutputStream(io.getOutputStream());

			channel.connect(60000);
			
			LOG.info("Connected over SSH to: " + host + " with user " + user);
		}
		
	}

}
