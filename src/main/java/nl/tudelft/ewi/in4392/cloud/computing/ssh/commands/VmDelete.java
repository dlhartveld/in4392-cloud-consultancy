package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

public class VmDelete extends Command<Boolean> {

	private final long id;

	public VmDelete(long id) {
		this.id = id;
	}

	@Override
	Boolean execute(InputOutput io) throws VmDeleteException {
		try {
			io.record(true);
			io.writeLine("onevm delete " + id);
			io.record(false);
			return true;
		} catch (IOException e) {
			throw new VmDeleteException(e);
		}
	}

	public class VmDeleteException extends RuntimeException {

		private static final long serialVersionUID = 7509232017910876912L;

		public VmDeleteException(Throwable e) {
			super(e);
		}

		public VmDeleteException(String message) {
			super(message);
		}

	}

}
