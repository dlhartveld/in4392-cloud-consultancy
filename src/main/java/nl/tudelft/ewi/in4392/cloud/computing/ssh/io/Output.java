package nl.tudelft.ewi.in4392.cloud.computing.ssh.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;

class Output {

	private final OutputStream out;
	private final BlockingQueue<Integer> queue;
	private boolean active = false;

	public Output() {
		queue = Queues.newArrayBlockingQueue(1000);

		out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				try {
					if (active) {
						queue.put(b);
					}
				}
				catch (InterruptedException e) {
					throw new IOException(e);
				}
			}
		};
	}

	public OutputStream getOutputStream() {
		return out;
	}

	public synchronized List<String> readResponse() throws IOException {
		try {
			StringBuilder builder = new StringBuilder();
			while (true) {
				Integer poll = queue.poll(1, TimeUnit.SECONDS);
				if (poll == null) {
					List<String> response = Lists.newArrayList(builder.toString().split("\r\n"));
					if (!response.isEmpty()) {
						response.remove(response.size() - 1);
					}
					return response;
				}

				int intValue = poll.intValue();
				builder.append((char) intValue);
			}
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

	public synchronized void close() throws IOException {
		out.close();
	}

	public synchronized void record(boolean active) throws IOException {
		this.active = active;

		try {
			while (queue.poll(1, TimeUnit.SECONDS) != null) {
			}
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

}
