package nl.tudelft.ewi.in4392.cloud.computing.comm;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import nl.tudelft.ewi.in4392.cloud.computing.git.RepoManager;
import nl.tudelft.ewi.in4392.cloud.computing.maven.Maven;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

@SuppressWarnings("serial")
public class BuildCommand implements Serializable {

	private static final Logger LOG = LoggerFactory.getLogger(BuildCommand.class);

	private String url;
	private CredentialsProvider credentials;

	public BuildCommand(String url) {
		this(url, null);
	}

	public BuildCommand(String url, CredentialsProvider credentials) {
		checkArgument(isNotEmpty(url), "url must be non-empty");

		this.url = url;
		this.credentials = credentials;
	}

	public String getGitUrl() {
		return url;
	}

	public void execute() {
		LOG.info("Executing build command for url: " + url);
		RepoManager repoManager = new RepoManager(Files.createTempDir(), credentials);
		Maven maven = new Maven(System.out, System.err);

		File workspace = repoManager.getWorkingDirectory();

		try {
			repoManager.clone(url);
		} catch (IOException e) {
			LOG.error("Failed to clone URL: {}", url, e);
			cleanUpQuietly(workspace);
			throw new BuildException("Failed to clone URL: " + url, e);
		}

		if (!maven.packageRepository(workspace)) {
			LOG.error("Failed to run maven build.");
			cleanUpQuietly(workspace);
			throw new BuildException("Maven build failed");
		}

		cleanUpQuietly(workspace);
	}

	private void cleanUpQuietly(File workspace) {
		try {
			FileUtils.deleteDirectory(workspace);
		} catch (IOException e) {
			LOG.warn("Failed to clean up workspace: " + workspace.getAbsolutePath(), e);
		}
	}

}
