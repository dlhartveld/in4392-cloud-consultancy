package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;
import java.util.List;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

import com.google.common.collect.Lists;

public class VmList extends Command<List<Vm>> {

	@Override
	List<Vm> execute(InputOutput io) throws VmListException {
		try {
			io.record(true);
			io.writeLine("onevm list");
			List<Vm> machines = list(io.readResponse());
			io.record(false);
			return machines;
		} catch (IOException e) {
			throw new VmListException(e);
		}
	}

	private List<Vm> list(List<String> lines) {
		List<Vm> machines = Lists.newArrayList();
		for (String line : lines) {
			line = line.trim();
			if (line.matches("^[0-9]+.*$")) {
				machines.add(parseLine(lines.get(1), line));
			}
		}
		return machines;
	}

	private Vm parseLine(String header, String line) {
		return new Vm(
				parse(header, "ID", true, line, Long.class),
				parse(header, "USER", false, line, String.class),
				parse(header, "GROUP", false, line, String.class),
				parse(header, "NAME", false, line, String.class),
				Vm.State.named(parse(header, "STAT", false, line, String.class)),
				cpu(parse(header, "CPU", true, line, String.class)),
				memory(parse(header, "MEM", true, line, String.class)),
				parse(header, "HOSTNAME", true, line, String.class),
				time(parse(header, "TIME", true, line, String.class)));
	}

	@SuppressWarnings("unchecked")
	private <T> T parse(String header, String field, boolean rightAligned, String input, Class<T> type) {
		Range location = locationOfHeader(header, field, rightAligned);
		String value = input.substring(location.start, location.end).trim();

		if (type.equals(Integer.class)) {
			return (T) (Integer) Integer.parseInt(value);
		}
		else if (type.equals(Long.class)) {
			return (T) (Long) Long.parseLong(value);
		}
		else if (type.equals(Double.class)) {
			return (T) (Double) Double.parseDouble(value);
		}
		else {
			return (T) value;
		}
	}

	private Range locationOfHeader(String header, String name, boolean rightAligned) {
		String tempHeader = header;
		tempHeader = tempHeader.substring(tempHeader.indexOf(' '));
		tempHeader = tempHeader.substring(0, tempHeader.indexOf('['));

		int start = tempHeader.indexOf(name) - 1;
		int end = tempHeader.indexOf(name) + name.length() - 1;
		if (rightAligned) {
			while (start > 0 && Character.isWhitespace(tempHeader.charAt(start - 1))) {
				start--;
			}
		}
		else {
			while (end < tempHeader.length() && Character.isWhitespace(tempHeader.charAt(end + 1))) {
				end++;
			}
		}

		return new Range(start, end);
	}

	private double cpu(String cpu) {
		return Double.parseDouble(cpu);
	}

	private long memory(String mem) {
		mem = mem.toLowerCase();
		double number = Double.parseDouble(mem.substring(0, mem.length() - 1));
		if (mem.endsWith("k")) {
			return (long) number;
		}
		else if (mem.endsWith("m")) {
			return (long) (number * 1024);
		}
		else if (mem.endsWith("g")) {
			return (long) (number * 1024 * 1024);
		}
		return -1;
	}

	private int time(String timestamp) {
		String days = timestamp.substring(0, timestamp.indexOf(' '));
		String time = timestamp.substring(timestamp.indexOf(' ') + 1, timestamp.length());

		days = days.trim();
		days = days.substring(0, days.length() - 1);

		int numDays = Integer.parseInt(days);
		int numHours = Integer.parseInt(time.substring(0, 2));
		int numMinutes = Integer.parseInt(time.substring(3, 5));
		return numDays * 24 * 3600 + numHours * 3600 + numMinutes * 60;
	}

	private class Range {
		private final int start;
		private final int end;

		public Range(int start, int end) {
			this.start = start;
			this.end = end;
		}
	}

	public class VmListException extends RuntimeException {

		private static final long serialVersionUID = 8904500504725606513L;

		public VmListException(Throwable e) {
			super(e);
		}

		public VmListException(String message) {
			super(message);
		}

	}

}
