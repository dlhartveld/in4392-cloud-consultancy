package nl.tudelft.ewi.in4392.cloud.computing.maven;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.apache.maven.cli.MavenCli;

public class Maven {

	private PrintStream out;
	private PrintStream err;

	public Maven(OutputStream out, OutputStream err) {
		this.out = dummyOrRealPrintStream(out);
		this.err = dummyOrRealPrintStream(err);
	}

	@SuppressWarnings("resource")
	private PrintStream dummyOrRealPrintStream(OutputStream out) {
		return out != null ? new PrintStream(out) : new PrintStream(new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				// Do nothing.
			}
		});
	}

	public boolean packageRepository(File workspace) {
		MavenCli cli = new MavenCli();

		int result = cli.doMain(new String[] {"package", "-e"}, workspace.getAbsolutePath(), out, err);
		return result == 0;
	}

}
