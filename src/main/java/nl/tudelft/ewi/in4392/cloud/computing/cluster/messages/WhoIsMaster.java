package nl.tudelft.ewi.in4392.cloud.computing.cluster.messages;

import java.io.Serializable;

import lombok.Data;

import org.jgroups.Address;

@Data
public class WhoIsMaster implements Serializable {

	private static final long serialVersionUID = -4861395401970213622L;

	private Address master;
	
}
