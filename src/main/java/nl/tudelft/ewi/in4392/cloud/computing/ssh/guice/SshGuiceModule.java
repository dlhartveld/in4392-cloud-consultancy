package nl.tudelft.ewi.in4392.cloud.computing.ssh.guice;

import java.util.Properties;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public class SshGuiceModule extends AbstractModule {

	private Properties properties;

	public SshGuiceModule(Properties properties) {
		this.properties = properties;
	}

	@Override
	protected void configure() {

	}

	@Provides
	@Singleton
	public Properties getProperties() {
		return properties;
	}

}
