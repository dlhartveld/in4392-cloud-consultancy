package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

public class VmSuspend extends Command<Boolean> {

	private final long id;

	public VmSuspend(long id) {
		this.id = id;
	}

	@Override
	Boolean execute(InputOutput io) throws VmSuspendException {
		try {
			io.record(true);
			io.writeLine("onevm suspend " + id);
			io.record(false);
			return true;
		} catch (IOException e) {
			throw new VmSuspendException(e);
		}
	}

	public class VmSuspendException extends RuntimeException {

		private static final long serialVersionUID = 7509232017910876912L;

		public VmSuspendException(Throwable e) {
			super(e);
		}

		public VmSuspendException(String message) {
			super(message);
		}

	}

}
