package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DisableFirewall extends Command<Boolean> {
	
	private static final Logger LOG = LoggerFactory.getLogger(DisableFirewall.class);

	private final String ipAddress;

	public DisableFirewall(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@Override
	Boolean execute(InputOutput io) throws DisableFirewallException {
		try {
			LOG.info("Disabling firewall at node: " + ipAddress);
			io.record(true);
			io.writeLine("ssh root@" + ipAddress + " \"service iptables stop\"");
			io.record(false);
			
			return true;
		} catch (IOException e) {
			throw new DisableFirewallException(e);
		}
	}

	public class DisableFirewallException extends RuntimeException {

		private static final long serialVersionUID = 8904500504725606513L;

		public DisableFirewallException(Throwable e) {
			super(e);
		}

		public DisableFirewallException(String message) {
			super(message);
		}

	}

}
