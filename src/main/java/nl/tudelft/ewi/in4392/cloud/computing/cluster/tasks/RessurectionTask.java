package nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import nl.tudelft.ewi.in4392.cloud.computing.cluster.ClusterIO;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Cluster;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Vm;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Vm.State;

import org.jgroups.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

public class RessurectionTask implements Runnable {
	
	private static final Logger LOG = LoggerFactory.getLogger(RessurectionTask.class);
	
	private final ClusterIO io;
	private final Cluster cluster;
	private final Set<Long> toLaunch;
	private final boolean devMode;
	
	private volatile int target = 3;
	
	@Inject
	public RessurectionTask(ClusterIO io, Cluster cluster, @Named("devMode") boolean devMode) {
		this.io = io;
		this.cluster = cluster;
		this.devMode = devMode;
		this.toLaunch = Sets.newHashSet();
	}
	
	public synchronized void setMinimumMachines(int minimum) {
		this.target = minimum;
	}
	
	public synchronized int getMinimumMachines() {
		return target;
	}

	@Override
	public void run() {
		View view = io.getView();
		List<Vm> machines = cluster.listMachines();
		
		int currentTarget = getMinimumMachines();
		
		if (!devMode) {
			int clusterSize = view.getMembers().size() + toLaunch.size();
			if (currentTarget > clusterSize) {
				LOG.info("Booting additional VMs: targetNumber: " + currentTarget + ", currentActive: " + clusterSize);
				int start = currentTarget - clusterSize;
				for (int i = 0; i < start; i++) {
					long vmId = cluster.bootMachine();
					LOG.info("Booting new VM: " + vmId);
					toLaunch.add(vmId);
					clusterSize++;
				}
			}
			
			for (Vm machine : machines) {
				String host = machine.getHost();
				State state = machine.getState();
				int uptime = machine.getUptime();
				boolean shouldBeLaunched = toLaunch.contains(machine.getId());
				
				if (host != null && !host.isEmpty() && state == State.ONLINE && uptime >= 120 && shouldBeLaunched) {
					LOG.info("Launching node on VM: " + machine.getId());
					String ipAddress = cluster.getIpAddress();
					cluster.disableFirewall(ipAddress);
					cluster.copyFileToNode(ipAddress, "config.properties");
					cluster.copyFileToNode(ipAddress, "node.jar");
					cluster.launchSystem(ipAddress);
					toLaunch.remove(machine.getId());
				}
			}
			
			for (Vm machine : machines) {
				if (machine.getState() == State.OFFLINE) {
					LOG.debug("Deleting VM: " + machine.getId());
					cluster.deleteMachine(machine.getId());
				}
			}
		}
	}
	
}
