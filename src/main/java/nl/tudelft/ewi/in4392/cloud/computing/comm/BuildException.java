package nl.tudelft.ewi.in4392.cloud.computing.comm;

@SuppressWarnings("serial")
public class BuildException extends RuntimeException {

	public BuildException(String message) {
		super(message);
	}

	public BuildException(String message, Throwable cause) {
		super(message, cause);
	}

}
