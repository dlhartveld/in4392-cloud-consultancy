package nl.tudelft.ewi.in4392.cloud.computing.main;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Properties;

import lombok.Cleanup;
import nl.tudelft.ewi.in4392.cloud.computing.cluster.ClusterIO;
import nl.tudelft.ewi.in4392.cloud.computing.cluster.RingClusterIO;
import nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks.JobDispatcher;
import nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks.JobSubmitter;
import nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks.MonitorTask;
import nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks.RessurectionTask;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.JobMgmtGuiceModule;
import nl.tudelft.ewi.in4392.cloud.computing.maven.MavenGuiceModule;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Cluster;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.DummyCluster;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.OpenNebulaCluster;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.guice.SshGuiceModule;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Scopes;
import com.google.inject.name.Names;

public class Boot {

	public static void main(final String[] args) throws Exception {
		final Properties config = loadProperties();

		System.setProperty("java.net.preferIPv4Stack", "true");
		final String ipAddress = getNetworkAddress(config.getProperty("interface"));
		System.setProperty("jgroups.bind_addr", ipAddress);

		SshGuiceModule sshModule = new SshGuiceModule(config);

		Guice.createInjector(sshModule, new MavenGuiceModule(), new JobMgmtGuiceModule(), new AbstractModule() {
			@Override
			protected void configure() {
				bind(Boolean.class).annotatedWith(Names.named("devMode")).toInstance(inDevMode(args));
				bind(String.class).annotatedWith(Names.named("imageLocation")).toInstance(config.getProperty("image"));
				bind(ClusterIO.class).to(RingClusterIO.class).in(Scopes.SINGLETON);
				bind(Bootstrapper.class).asEagerSingleton();

				bind(JobDispatcher.class).asEagerSingleton();
				bind(MonitorTask.class).asEagerSingleton();
				bind(RessurectionTask.class).asEagerSingleton();
				bind(JobSubmitter.class).asEagerSingleton();
				
				bind(String.class).annotatedWith(Names.named("ipAddress")).toInstance(ipAddress);

				if (inDevMode(args)) {
					bind(Cluster.class).to(DummyCluster.class);
				}
				else {
					bind(Cluster.class).to(OpenNebulaCluster.class);
				}
			}
		});
	}

	private static boolean inDevMode(String[] args) {
		return args.length > 0 && "devMode".equals(args[0]);
	}

	private static Properties loadProperties() throws IOException {
		Properties properties = new Properties();
		@Cleanup
		InputStream resourceAsStream = Boot.class.getResourceAsStream("/config.properties");
		if (resourceAsStream != null) {
			properties.load(resourceAsStream);
		}

		File file = new File("config.properties");
		if (file.exists()) {
			properties.load(new FileReader(file));
		}

		return properties;
	}

	private static String getNetworkAddress(String interfaceName) throws SocketException {
		Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
		while (networks.hasMoreElements()) {
			NetworkInterface network = networks.nextElement();
			if (interfaceName.equalsIgnoreCase(network.getName())) {
				return getFirstIPv4Address(network);
			}
		}
		throw new IllegalArgumentException("Could not locate network interface with name: " + interfaceName);
	}

	private static String getFirstIPv4Address(NetworkInterface network) {
		Enumeration<InetAddress> addresses = network.getInetAddresses();
		while (addresses.hasMoreElements()) {
			String hostAddress = addresses.nextElement().getHostAddress();
			if (hostAddress.contains(".")) {
				return hostAddress;
			}
		}
		throw new IllegalArgumentException("Could not retrieve first IP of interface: " + network.getName());
	}

}
