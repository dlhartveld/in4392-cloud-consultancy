package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.eclipse.jgit.transport.CredentialsProvider;

@SuppressWarnings("serial")
public class Job implements Serializable {

	private User user;
	private String gitUrl;
	private CredentialsProvider gitCredentials;

	private long startTime;
	private long completionTime;

	public Job(User user, String gitUrl, CredentialsProvider gitCredentials) {
		checkArgument(isNotEmpty(gitUrl), "gitUrl must be non-empty");

		this.user = checkNotNull(user, "user");
		this.gitUrl = gitUrl;
		this.gitCredentials = gitCredentials;

		startTime = -1;
		completionTime = -1;
	}

	public User getUser() {
		return user;
	}

	public String getGitUrl() {
		return gitUrl;
	}

	public CredentialsProvider getGitCredentials() {
		return gitCredentials;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		checkState(this.startTime == -1, "Job is already marked as started");

		checkArgument(startTime > 0, "startTime must be > 0");

		this.startTime = startTime;
	}

	public long getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(long completionTime) {
		checkState(isStarted(), "Job is not yet started");
		checkState(this.completionTime == -1, "Job is already marked as completed");

		checkArgument(completionTime > startTime, "completionTime must be > startTime");

		this.completionTime = completionTime;
	}

	public boolean isRunning() {
		return isStarted() && !isStopped();
	}

	public boolean isCompleted() {
		return isStarted() && isStopped();
	}

	private boolean isStarted() {
		return startTime > 0;
	}

	private boolean isStopped() {
		return completionTime > startTime;
	}

	public long getRuntime() {
		checkState(isCompleted(), "Job is not yet completed");

		return completionTime - startTime;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		builder.append("user", user);
		builder.append("gitUrl", gitUrl);
		builder.append("startTime", startTime);
		builder.append("completionTime", completionTime);
		return builder.toString();
	}

}
