package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Cluster;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class JobTrackerFactory {

	private static final Logger LOG = LoggerFactory.getLogger(JobTrackerFactory.class);

	private final String path;
	private final Cluster cluster;

	@Inject
	public JobTrackerFactory(Cluster cluster, @Named("jobtracker.data") String path) {
		this.cluster = cluster;
		LOG.trace("Creating JobTrackerFactory with data file path: {}", path);

		checkArgument(isNotEmpty(path), "path must be non-empty");

		this.path = path;
	}

	public synchronized JobTracker deserialize() {

		LOG.trace("Deserializing from: {}", path);
		
		// Transfer file from file server to local node...
		cluster.copyFileToNode(cluster.getIpAddress(), path);

		ObjectInputStream s = null;
		Object object = null;
		try {
			s = new ObjectInputStream(new FileInputStream(path));

			object = s.readObject();
		} catch (FileNotFoundException e) {
			LOG.warn("Could not locate jobs on disk!");
			return new JobTracker(cluster, path);
		} catch (IOException e) {
			throw new RuntimeException("Deserialization failure", e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Could not find Class", e);
		} finally {
			IOUtils.closeQuietly(s);
		}

		if (object instanceof JobTracker) {
			LOG.trace("Deserialized JobTracker: {}", object);
			JobTracker tracker = (JobTracker) object;
			tracker.setCluster(cluster);
			return tracker;
		} else {
			throw new RuntimeException("Expected JobTracker, but got some other object: " + object);
		}
	}

	public synchronized void serialize(JobTracker jobTracker) {

		ObjectOutputStream s = null;
		try {
			s = new ObjectOutputStream(new FileOutputStream(path));
			s.writeObject(jobTracker);
			s.flush();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Could not open file", e);
		} catch (IOException e) {
			throw new RuntimeException("Serialization failure", e);
		} finally {
			IOUtils.closeQuietly(s);
		}
		
		// Transfer file to file server...
		cluster.copyFileFromNode(cluster.getIpAddress(), path);
	}
}
