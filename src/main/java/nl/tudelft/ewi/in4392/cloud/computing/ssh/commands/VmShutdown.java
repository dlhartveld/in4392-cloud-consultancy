package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

public class VmShutdown extends Command<Boolean> {

	private final long id;

	public VmShutdown(long id) {
		this.id = id;
	}

	@Override
	Boolean execute(InputOutput io) throws VmShutdownException {
		try {
			io.record(true);
			io.writeLine("onevm shutdown " + id);
			io.record(false);
			return true;
		} catch (IOException e) {
			throw new VmShutdownException(e);
		}
	}

	public class VmShutdownException extends RuntimeException {

		private static final long serialVersionUID = 7509232017910876912L;

		public VmShutdownException(Throwable e) {
			super(e);
		}

		public VmShutdownException(String message) {
			super(message);
		}

	}

}
