package nl.tudelft.ewi.in4392.cloud.computing.maven;

import java.io.OutputStream;

import javax.inject.Named;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public class MavenGuiceModule extends AbstractModule {

	@Override
	protected void configure() {

	}

	@Provides
	@Named("maven.out")
	public OutputStream getMavenStandardOutputStream() {
		return System.out;
	}

	@Provides
	@Named("maven.err")
	public OutputStream getMavenErrorOutputStream() {
		return System.err;
	}

}
