package nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import nl.tudelft.ewi.in4392.cloud.computing.cluster.ClusterIO;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.JobTracker;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.TrackerStatus;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Cluster;
import nl.tudelft.ewi.in4392.cloud.computing.ssh.commands.Vm;

import org.jgroups.Address;
import org.jgroups.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MonitorTask implements Runnable {
	
	private static final String STATE_FILE = "state.log";

	private static final Logger LOG = LoggerFactory.getLogger(MonitorTask.class);
	
	private final ClusterIO io;
	private final Cluster cluster;

	private JobTracker tracker;

	@Inject
	public MonitorTask(ClusterIO io, Cluster cluster) {
		this.io = io;
		this.cluster = cluster;
	}
	
	public void setJobTracker(JobTracker tracker) {
		this.tracker = tracker;
	}

	@Override
	public void run() {
		StringBuilder builder = new StringBuilder();
		View view = io.getView();
		Address master = io.getMaster();
		List<Vm> machines = cluster.listMachines();
		
		builder.append("OpenNebula network:\n");
		for (Vm machine : machines) {
			builder.append("  " + machine.getId() + "\t" + machine.getState() + "\t uptime: " + machine.getUptime() + "s\n");
		}
		
		builder.append("\nJGroups network:\n");
		TrackerStatus inspect = tracker.inspect();
		for (Address address : view) {
			if (address.equals(master)) {
				builder.append("  MASTER :\t");
			}
			else {
				builder.append("  SLAVE  : \t");
			}
			
			builder.append(address);
			if (tracker != null && inspect.getBusyNodes().contains(address)) {
				builder.append("\t (busy)");
			}
			
			builder.append("\n");
		}
		
		if (tracker != null) {
			builder.append("\nJobs in progress : " + inspect.getNumJobsInProgress());
			builder.append("\nJobs in queue    : " + inspect.getQueueSize() + "\n");
		}
		
		try {
			FileWriter writer = new FileWriter(new File(STATE_FILE));
			writer.write(builder.toString());
			writer.close();
		}
		catch (IOException e) {
			LOG.warn(e.getMessage(), e);
		}
		
		cluster.copyFileFromNode(cluster.getIpAddress(), STATE_FILE);
	}

}
