package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Launch extends Command<Boolean> {
	
	private static final Logger LOG = LoggerFactory.getLogger(Launch.class);

	private final String ipAddress;
	private final String jarName;

	public Launch(String ipAddress, String jarName) {
		this.ipAddress = ipAddress;
		this.jarName = jarName;
	}

	@Override
	Boolean execute(InputOutput io) throws LaunchException {
		try {
			LOG.info("Starting code on remote node: " + ipAddress);
			io.record(true);
			io.writeLine("ssh root@" + ipAddress + " \"java -jar " + jarName + " < /dev/null > log.txt 2>&1 &\"");
			io.record(false);
			return true;
		} catch (IOException e) {
			throw new LaunchException(e);
		}
	}

	public class LaunchException extends RuntimeException {

		private static final long serialVersionUID = 8904500504725606513L;

		public LaunchException(Throwable e) {
			super(e);
		}

		public LaunchException(String message) {
			super(message);
		}

	}

}
