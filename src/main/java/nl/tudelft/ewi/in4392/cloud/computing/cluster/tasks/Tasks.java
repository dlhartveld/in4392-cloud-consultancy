package nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks;

import javax.inject.Inject;

import nl.tudelft.ewi.in4392.cloud.computing.jobs.JobTracker;

public class Tasks {

	private final MonitorTask monitor;
	private final RessurectionTask ressurector;
	private final JobDispatcher dispatcher;
	private final JobSubmitter submitter;

	@Inject
	public Tasks(MonitorTask monitor, RessurectionTask ressurector, JobDispatcher dispatcher, JobSubmitter submitter) {
		this.monitor = monitor;
		this.ressurector = ressurector;
		this.dispatcher = dispatcher;
		this.submitter = submitter;
	}
	
	public MonitorTask getClusterHealthMonitor(JobTracker jobTracker) {
		monitor.setJobTracker(jobTracker);
		return monitor;
	}
	
	public RessurectionTask getNodeRessurector() {
		return ressurector;
	}
	
	public JobDispatcher getJobDispatcher(JobTracker jobTracker) {
		dispatcher.setJobTracker(jobTracker);
		return dispatcher;
	}

	public JobSubmitter getJobSubmitter(JobTracker jobTracker) {
		submitter.setJobTracker(jobTracker);
		return submitter;
	}
	
}
