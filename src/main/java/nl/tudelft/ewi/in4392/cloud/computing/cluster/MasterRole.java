package nl.tudelft.ewi.in4392.cloud.computing.cluster;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks.JobDispatcher;
import nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks.Tasks;
import nl.tudelft.ewi.in4392.cloud.computing.comm.BuildResult;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.JobTracker;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.JobTrackerFactory;

import org.jgroups.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MasterRole implements Role {

	private static final Logger LOG = LoggerFactory.getLogger(MasterRole.class);
	
	private final ScheduledThreadPoolExecutor executor;
	private final Tasks tasks;

	private JobTrackerFactory jobTrackerFactory;
	private JobDispatcher dispatcher = null;
	
	@Inject
	public MasterRole(ClusterIO io, JobTrackerFactory jobTrackerFactory, Tasks tasks) {
		this.jobTrackerFactory = jobTrackerFactory;
		this.tasks = tasks;
		this.executor = new ScheduledThreadPoolExecutor(4);
	}

	@Override
	public void onStart() {
		LOG.info("Assuming MASTER role...");
		JobTracker jobTracker = jobTrackerFactory.deserialize();
		executor.scheduleWithFixedDelay(tasks.getClusterHealthMonitor(jobTracker), 5, 5, TimeUnit.SECONDS);
		executor.scheduleWithFixedDelay(tasks.getNodeRessurector(), 10, 15, TimeUnit.SECONDS);
		executor.scheduleWithFixedDelay(tasks.getJobSubmitter(jobTracker), 5, 60, TimeUnit.SECONDS);
		executor.scheduleWithFixedDelay(tasks.getJobDispatcher(jobTracker), 15, 5, TimeUnit.SECONDS);
	}

	@Override
	public void onMessage(Address src, Container content) {
		LOG.trace("Received message from {} with content: {}", src, content);

		if (content.isA(BuildResult.class)) {
			LOG.info("Processing build result.");
			BuildResult result = content.getContent();
			dispatcher.onJobCompletion(src, result);
		} else {
			LOG.debug("Unknown message: {} - from: {}", content, src);
		}
	}

	@Override
	public void onTerminate() {
		LOG.info("Releasing MASTER role...");
		executor.shutdownNow();
	}

	@Override
	public void onElectionStart() {
		/*
		 * Do nothing. If this happens, it means the cluster could not reach
		 * this node, and has issued a master election process.
		 */
	}

	@Override
	public void onElectionEnd(Address master) {
		/*
		 * Do nothing. If this happens, it means the cluster could not reach
		 * this node, and has issued a master election process.
		 */
	}

}
