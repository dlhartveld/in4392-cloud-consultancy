package nl.tudelft.ewi.in4392.cloud.computing.cluster.tasks;

import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;

import nl.tudelft.ewi.in4392.cloud.computing.cluster.ClusterIO;
import nl.tudelft.ewi.in4392.cloud.computing.comm.BuildCommand;
import nl.tudelft.ewi.in4392.cloud.computing.comm.BuildResult;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.Job;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.JobTracker;
import nl.tudelft.ewi.in4392.cloud.computing.jobs.TrackerStatus;

import org.jgroups.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

public class JobDispatcher implements Runnable {
	
	private static final Logger LOG = LoggerFactory.getLogger(JobDispatcher.class);
	
	private final RessurectionTask nodeRessurector;
	private final ClusterIO io;
	
	private JobTracker jobTracker;

	@Inject
	public JobDispatcher(ClusterIO io, RessurectionTask nodeRessurector) {
		this.io = io;
		this.nodeRessurector = nodeRessurector;
	}
	
	public void setJobTracker(JobTracker tracker) {
		this.jobTracker = tracker;
	}

	@Override
	public void run() {
		TrackerStatus inspect = jobTracker.inspect();
		int queueSize = inspect.getQueueSize();
		
		Set<Address> idleNodes = Sets.newHashSet(io.getView().getMembers());
		idleNodes.remove(io.getMaster());
		idleNodes.removeAll(inspect.getBusyNodes());
		
		int availableNodes = idleNodes.size();
		
		if (queueSize > 0) {
			if (queueSize > availableNodes) {
				LOG.info("Adjusting cluster size to meet queue requirements...");
				nodeRessurector.setMinimumMachines(inspect.getNumJobsInProgress() + inspect.getQueueSize() + 1);
			}
			
			Iterator<Address> iterator = idleNodes.iterator();
			while (jobTracker.inspect().getQueueSize() > 0 && iterator.hasNext()) {
				Address node = iterator.next();
				LOG.info("Dispatching job to node: " + node);
				Job job = jobTracker.takeForExecutionOn(node);
				io.send(node, new BuildCommand(job.getGitUrl(), job.getGitCredentials()));
			}
		}
	}
	
	public synchronized void onJobCompletion(Address src, BuildResult result) {
		jobTracker.onCompletionOf(src);
	}

}
