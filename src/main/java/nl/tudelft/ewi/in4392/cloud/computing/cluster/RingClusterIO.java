package nl.tudelft.ewi.in4392.cloud.computing.cluster;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import nl.tudelft.ewi.in4392.cloud.computing.cluster.messages.Election;
import nl.tudelft.ewi.in4392.cloud.computing.cluster.messages.WhoIsMaster;

import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class RingClusterIO implements ClusterIO {
	
	private static final Logger LOG = LoggerFactory.getLogger(RingClusterIO.class);
	
	private static final Object lock = new Object();
	
	private final ClusterRolManager listener;
	private final MasterRole masterRole;
	private final SlaveRole slaveRole;
	
	private JChannel channel = null;
	private Role currentRole = null;
	private Address currentMaster = null;
	private boolean inElection = false;
	
	@Inject
	public RingClusterIO(MasterRole master, SlaveRole slave) {
		this.listener = new ClusterRolManager();
		this.masterRole = master;
		this.slaveRole = slave;
	}
	
	private boolean isConnected() {
		return channel != null && channel.isConnected();
	}

	private Address connect() throws Exception {
		if (channel == null) {
			channel = new JChannel();
	        channel.setReceiver(listener);
	        channel.connect("cloud");
		}
		return getAddress();
	}
	
	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.cluster.ClisterIO#ensureConnected()
	 */
	@Override
	public synchronized Address ensureConnected() throws Exception {
		if (!isConnected()) {
			return connect();
		}
		return getAddress();
	}
	
	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.cluster.ClisterIO#disconnect()
	 */
	@Override
	public synchronized void disconnect() {
		if (channel != null) {
			channel.disconnect();
			channel = null;
		}
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.cluster.ClisterIO#getView()
	 */
	@Override
	public View getView() {
		return channel.getView();
	}
	
	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.cluster.ClisterIO#getAddress()
	 */
	@Override
	public Address getAddress() {
		return channel.getAddress();
	}
	
	@Override
	public String getHostName() {
		String host = getAddress().toString();
		return host.substring(0, host.lastIndexOf('-'));
	}

	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.cluster.ClisterIO#getMaster()
	 */
	@Override
	public Address getMaster() {
		return currentMaster;
	}
	
	/* (non-Javadoc)
	 * @see nl.tudelft.ewi.in4392.cloud.computing.cluster.ClisterIO#send(org.jgroups.Address, java.lang.Object)
	 */
	@Override
	public synchronized void send(Address to, Object o) {
		try {
			channel.send(to, new Container(o.getClass(), o));
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	private class ClusterRolManager extends ReceiverAdapter {
		
		@Override
		public void receive(Message msg) {
			Container container = (Container) msg.getObject();
			if (container.isA(WhoIsMaster.class)) {
				WhoIsMaster message = container.getContent();
				if (message.getMaster() == null) {
					LOG.debug("Replying to request for master's address...");
					message.setMaster(currentMaster);
					send(msg.getSrc(), message);
				}
				else {
					LOG.debug("Received master's address...");
					currentMaster = message.getMaster();
					if (currentMaster.equals(getAddress())) {
						switchToRole(masterRole);
					}
					else {
						switchToRole(slaveRole);
					}
				}
			}
			else if (container.isA(Election.class)) {
				Election info = container.getContent();
				
				// Wait for the view to update.
				while (getView().getViewId().getId() < info.getViewTime()) {
					try {
						Thread.sleep(100);
					} 
					catch (InterruptedException e) {
						// Do nothing.
					}
				}
				
				if (info.getSender().equals(getAddress())) {
					inElection = false;
					currentMaster = info.getProposedMaster();
					LOG.debug("Result of master election: " + currentMaster);
					
					if (currentRole != null) {
						currentRole.onElectionEnd(currentMaster);
					}
					
					if (currentMaster.equals(getAddress())) {
						switchToRole(masterRole);
					}
					else {
						switchToRole(slaveRole);
					}
				}
				else {
					if (!inElection) {
						inElection = true;
						LOG.debug("Entering master election...");
						if (currentRole != null) {
							currentRole.onElectionStart();
						}
					}
					
					if (info.getProposedMaster().compareTo(getAddress()) < 0) {
						info.setProposedMaster(getAddress());
					}
					LOG.debug("Casting vote in master election...");
					send(getNeighbor(getView()), info);
				}
			}
			else {
				synchronized (lock) {
					currentRole.onMessage(msg.getSrc(), container);
				}
			}
		}
		
		@Override
		public void viewAccepted(View view) {
			LOG.info("Cluster changed: Now " + view.getMembers().size() + " members...");
			
			if (view.getMembers().size() > 1) {
				if (currentMaster == null) {
					LOG.debug("Asking neighbor for master's address...");
					send(getNeighbor(view), new WhoIsMaster());
				}
				else if (!view.containsMember(currentMaster) && !inElection) {
					LOG.debug("Issueing new master election...");
					send(getNeighbor(view), new Election(getAddress(), view.getVid().getId(), getAddress()));
				}
			}
			else {
				LOG.debug("No other nodes found...");
				currentMaster = getAddress();
				switchToRole(masterRole);
			}
		}

		private Address getNeighbor(View view) {
			List<Address> members = Lists.newArrayList(view.getMembers());
			Collections.sort(members);
			
			int myIndex = members.indexOf(getAddress());
			return members.get((myIndex + 1) % members.size());
		}

		private void switchToRole(Role role) {
			synchronized (lock) {
				if (currentRole == null) {
					currentRole = role;
					currentRole.onStart();
				}
				else if (!currentRole.getClass().equals(role.getClass())) {
					currentRole.onTerminate();
					currentRole = role;
					currentRole.onStart();
				}
			}
		}
	}
	
}
