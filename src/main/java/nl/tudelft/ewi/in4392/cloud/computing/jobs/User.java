package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@SuppressWarnings("serial")
public class User implements Serializable {

	private long id;
	private String name;

	public User(long id, String name) {
		checkArgument(id >= 0, "id must be >= 0");
		checkArgument(isNotEmpty(name), "name must be non-empty");

		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		builder.append("id", id);
		builder.append("name", name);
		return builder.toString();
	}

}
