package nl.tudelft.ewi.in4392.cloud.computing.jobs;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.jgroups.Address;

import com.rits.cloning.Cloner;

public class TrackerStatus {

	private final Collection<Job> queue;
	private final Map<Address, Job> inProgress;
	private final Map<User, List<Job>> jobs;

	TrackerStatus(Collection<Job> queue, Map<Address, Job> inProgress, Map<User, List<Job>> jobs) {
		Cloner cloner = new Cloner();
		this.queue = cloner.deepClone(queue);
		this.inProgress = cloner.deepClone(inProgress);
		this.jobs = cloner.deepClone(jobs);
	}

	public int getQueueSize() {
		return queue.size();
	}

	public int getNumJobsInProgress() {
		return inProgress.size();
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		builder.append("queue", queue);
		builder.append("inProgress", inProgress);
		builder.append("jobs", jobs);
		return builder.toString();
	}

	public Set<Address> getBusyNodes() {
		return inProgress.keySet();
	};

}
