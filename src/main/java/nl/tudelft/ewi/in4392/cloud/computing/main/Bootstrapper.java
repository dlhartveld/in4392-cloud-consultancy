package nl.tudelft.ewi.in4392.cloud.computing.main;

import javax.inject.Inject;

import nl.tudelft.ewi.in4392.cloud.computing.cluster.ClusterIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Bootstrapper {
	
	private static final Logger LOG = LoggerFactory.getLogger(Bootstrapper.class);
	
	@Inject
	public Bootstrapper(ClusterIO cluster) {
		try {
			cluster.ensureConnected();
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
}
