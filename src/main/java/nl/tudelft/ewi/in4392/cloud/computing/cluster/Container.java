package nl.tudelft.ewi.in4392.cloud.computing.cluster;

import java.io.Serializable;

import lombok.Data;

@Data
public class Container implements Serializable {
	private static final long serialVersionUID = 9067717044596984121L;
	
	private final Class<?> clazz;
	private final Object content;
	
	@SuppressWarnings("unchecked")
	public <T> T getContent() {
		return (T) content;
	}
	
	public boolean isA(Class<?> other) {
		return clazz.isAssignableFrom(other);
	}
}