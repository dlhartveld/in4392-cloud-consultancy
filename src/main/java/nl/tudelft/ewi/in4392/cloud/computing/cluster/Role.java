package nl.tudelft.ewi.in4392.cloud.computing.cluster;

import org.jgroups.Address;

public interface Role {

	void onStart();
	
	void onMessage(Address from, Container container);
	
	void onTerminate();
	
	void onElectionStart();
	
	void onElectionEnd(Address master);
	
}
