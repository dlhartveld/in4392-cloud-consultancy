package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.util.List;

public interface Cluster {

	public abstract List<Vm> listMachines();

	public abstract long bootMachine();

	public abstract boolean launchSystem(String ipAddress);

	public abstract void shutdownMachine(long vmId);
	
	public abstract void shutdownMachine();

	public abstract void deleteMachine(long vmId);

	public abstract void disconnect();

	public abstract String identify(long id);

	public abstract boolean copyFileToNode(String ipAddress, String file);
	public abstract boolean copyFileFromNode(String ipAddress, String file);

	public abstract void disableFirewall(String ipAddress);

	public abstract String getIpAddress();

}