package nl.tudelft.ewi.in4392.cloud.computing.comm;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class BuildResult implements Serializable {

	private String url;
	private boolean success;

}
