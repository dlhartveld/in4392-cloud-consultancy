package nl.tudelft.ewi.in4392.cloud.computing.git;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.transport.CredentialsProvider;

import com.google.common.base.Preconditions;

public class RepoManager {

	private final File workingDirectory;
	private final CredentialsProvider credentialProvider;
	
	private Git git;

	public RepoManager(File workingDirectory, CredentialsProvider credentialProvider) {
		Preconditions.checkNotNull(workingDirectory);
		this.workingDirectory = workingDirectory;
		this.credentialProvider = credentialProvider;
	}
	
	public void open() throws IOException {
		this.git = Git.open(workingDirectory);
	}
	
	public ObjectId lastCommit() throws IOException {
		LogCommand log = git.log();
		log.setMaxCount(1);
		try {
			return log.call().iterator().next().getId();
		} catch (NoHeadException e) {
			throw new IOException(e);
		} catch (GitAPIException e) {
			throw new IOException(e);
		}
	}
	
	public void clone(String uri) throws IOException {
		Preconditions.checkNotNull(uri);
		
		CloneCommand clone = Git.cloneRepository();
		clone.setDirectory(workingDirectory);
		clone.setURI(uri);
		clone.setCredentialsProvider(credentialProvider);
		try {
			this.git = clone.call();
		} catch (InvalidRemoteException e) {
			throw new IOException(e);
		} catch (TransportException e) {
			throw new IOException(e);
		} catch (GitAPIException e) {
			throw new IOException(e);
		}
	}
	
	public void init() throws IOException {
		InitCommand initCommand = Git.init();
		initCommand.setDirectory(workingDirectory);
		try {
			this.git = initCommand.call();
		} catch (JGitInternalException e) {
			throw new IOException(e);
		} catch (GitAPIException e) {
			throw new IOException(e);
		}
	}
	
	public File getWorkingDirectory() {
		return workingDirectory;
	}

}
