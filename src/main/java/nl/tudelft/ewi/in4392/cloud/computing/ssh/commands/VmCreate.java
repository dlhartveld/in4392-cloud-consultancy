package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;
import java.util.List;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

public class VmCreate extends Command<Long> {

	private final String imageLocation;

	public VmCreate(String imageLocation) {
		this.imageLocation = imageLocation;
	}

	@Override
	Long execute(InputOutput io) throws VmCreateException {
		try {
			io.record(true);
			io.writeLine("onevm create " + imageLocation);
			List<String> results = io.readResponse();
			io.record(false);

			for (String result : results) {
				if (result != null && result.startsWith("ID:")) {
					String id = result.substring(3).trim();
					if (id != null && !id.isEmpty() && id.matches("[0-9]+")) {
						return Long.parseLong(id);
					}
				}
			}

			throw new VmCreateException("Could not create VM: " + results);
		} catch (IOException e) {
			throw new VmCreateException(e);
		}
	}

	public class VmCreateException extends RuntimeException {

		private static final long serialVersionUID = 8904500504725606513L;

		public VmCreateException(Throwable e) {
			super(e);
		}

		public VmCreateException(String message) {
			super(message);
		}

	}

}
