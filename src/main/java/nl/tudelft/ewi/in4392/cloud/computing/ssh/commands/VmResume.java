package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

public class VmResume extends Command<Boolean> {

	private final long id;

	public VmResume(long id) {
		this.id = id;
	}

	@Override
	Boolean execute(InputOutput io) throws VmResumeException {
		try {
			io.record(true);
			io.writeLine("onevm resume " + id);
			io.record(false);
			return true;
		} catch (IOException e) {
			throw new VmResumeException(e);
		}
	}

	public class VmResumeException extends RuntimeException {

		private static final long serialVersionUID = 7509232017910876912L;

		public VmResumeException(Throwable e) {
			super(e);
		}

		public VmResumeException(String message) {
			super(message);
		}

	}

}
