package nl.tudelft.ewi.in4392.cloud.computing.ssh.commands;

import java.io.IOException;
import java.util.List;

import nl.tudelft.ewi.in4392.cloud.computing.ssh.io.InputOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CopyFileTo extends Command<Boolean> {
	
	private static final Logger LOG = LoggerFactory.getLogger(CopyFileTo.class);
	
	private final String ipAddress;
	private final String fileName;
	
	public CopyFileTo(String ipAddress, String fileName) {
		this.ipAddress = ipAddress;
		this.fileName = fileName;
	}

	@Override
	Boolean execute(InputOutput io) {
		try {
			boolean retry = true;
			while (retry) {
				LOG.info("Copying file: " + fileName + " from: " + ipAddress);
				io.record(true);
				io.writeLine("scp " + fileName + " root@" + ipAddress + ":" + fileName);
				List<String> responses = io.readResponse();
				io.record(false);
				if (!responses.contains("lost connection")) {
					retry = false;
				}
			}
			return true;
		}
		catch (IOException e) {
			return false;
		}
	}

}
