\documentclass[12pt,a4paper]{IEEEtran}

\begin{document}

\title{
	Investigating elasticity for build infrastructures\\
	WantCloud BV\\
	\large IN4392 Cloud Computing Lab Report
}


\author{
\IEEEauthorblockN{D.L. Hartveld - Student - 1181092}
\IEEEauthorblockA{\\d.l.hartveld@student.tudelft.nl\\}
\and
\IEEEauthorblockN{M. de Jong - Student - 1331479}
\IEEEauthorblockA{\\m.dejong-2@student.tudelft.nl\\}
\and
\IEEEauthorblockN{D.H.J. Epema - Course Instructor}
\IEEEauthorblockA{\\d.h.j.epema@tudelft.nl\\}
\and
\IEEEauthorblockN{A. Iosup - Course Instructor}
\IEEEauthorblockA{\\a.iosup@tudelft.nl\\}
\and
\IEEEauthorblockN{B.I. Ghit - Teaching assistant}
\IEEEauthorblockA{\\b.i.ghit@tudelft.nl\\}
}

\maketitle

\begin{abstract}
In this report we explore the possibilities of Cloud Computing for WantCloud BV, 
and present a system to replace the current build infrastructure at WantCloud BV. 
This system is capable of running on a OpenNebula cluster, and will automatically 
scale itself to the required build capacity. We explain how this system works 
and operates, and briefly cover some of our experiments that we ran on this 
system.
\end{abstract}



\section{Introduction: Managing an elastic build infrastructure}
\label{sec:introduction}
%Describe the problem, the existing systems and/or tools (related work), the
%system you are about to implement, and the structure of the remainder of the
%article; use one short paragraph for each. (recommended size, including points 1
%and 2: 1 page.)

WantCloud BV has asked for an investigation of IaaS Cloud Computing concepts,
including an evaluation of the possibilities and opportunities of implementing
an IaaS-based cloud computing model for the build infrastructure service of WantCloud.

The build infrastructure of WantCloud currently consists of several servers that
run maven\footnote{Apache Maven: http://maven.apache.org/} build jobs on request.
However, these systems are idle for a significant
amount of time, and sometimes have long queues when users submit many jobs at
the same time. WantCloud expects their build service business to show strong
growth in the next year or two, and wants to prepare for a sharp uptake of its
service. The current business model is inadequate because growth requires the
roll-out of many additional physical servers, including the need to maintain
those servers. The elasticity of the cloud could provide an interesting
alternative to the use of physical servers. Through the use of automatic scaling
of the build server park, more efficient use of resources could be obtained.

This report describes an exploration of a simple use case in which maven project
builds are executed on an elastic IaaS based on OpenNebula\footnote{OpenNebula Data Center Virtualization: http://opennebula.org/},
as it is provided by the DAS-4 distributed system\footnote{Distributed ASCI Supercomputer 4: http://www.cs.vu.nl/das4/}.

The next part, section \ref{sec:app-desc} of this report describes the
application and its requirements in more detail. Section \ref{sec:sys-design}
describes the architecture of our experimental software solution. The results of
executed experiments can be found in section \ref{sec:results}. Section
\ref{sec:discussion} discusses these results. The conclusions can be found in
section \ref{sec:conclusion}.

Finally, the source code of the experimental implementation of the build
infrastructure service can be found in a Git repository on Bitbucket\footnote{https://bitbucket.org/dlhartveld/in4392-cloud-consultancy}.

\section{Application description}
\label{sec:app-desc}
%Background on Application (recommended size: 0.5 pages): describe the application (1 
%paragraph) and its requirements (1-3 paragraphs, summarized in a table if needed).

We choose to design and build a system which periodically checks source code
repositories and attempts to build the contents of these repositories. Such a system is better known
as a Continuous Integration system. These systems are meant to notify developers
when they commit broken code into the repository, by continuously trying to
build the code and running the associated unit and integration tests. If the build or tests
fail, the responsible developer is notified automatically using e-mail.

Load for such systems is highly erratic, since it only pays to build and test
code after a developer has made a change to the previous version. This means
that during the day the system will be under regular load, whilst at night
the cluster is reduced to the bare minimum since it does not need to build or
test the code since all the developers are asleep. This process should obviously
be fully automated since we do not want to scale this system manually. 
Additionally this requires elasticity, reliability, and scheduling. 

We want to be able to serve different users with the same system, 
so we need multitenancy built right in. We could even apply user profiling to see 
how much resources are consumed by each user, and plan accordingly.

\section{System Design}
\label{sec:sys-design}

\subsection{Resource Management Architecture}
\label{sec:architecture}
%Describe the design of your system, including the inter-operation of the
%provisioning,  allocation, reliability, and monitoring components (which
%correspond to the homonym features required by the WantCloud CTO).
Since we choose to build our system on the DAS-4 cluster, which uses OpenNebula 
we needed a way to provision and manage virtual machines in that cluster. The 
very brief documentation shows how to manage virtual machines in the cluster 
using some very simple commands using SSH (Secure Shell). So in order to 
actually manage the cluster from within our system we wrote a small wrapper in 
Java which alllowed us to monitor, request, shutdown and delete virtual machines. 
This wrapper simply sends the documented OpenNebula commands over SSH to 
manage the cluster, and if required, reads the output of these commands. Later on 
we extended this to be able to copy files between nodes, disable firewalls, and 
start the application itself.

\begin{itemize}
  \item Automation
One of the most obvious requirements is that this system should be fully 
automated. We achieved this through reliability and 
elasticity. In our system there is always a special node called master. This master 
is capable of monitoring the cluster and ensuring that the cluster remains in 
good health and essentially does not stop running as long as the underlying 
OpenNebula system is running. To enable communication between nodes 
we have added a special library for Java to the project called: 
JGroups\footnote{http://www.jgroups.org/}. The JGroups 
library automatically discovers other nodes on the network, and allows our 
nodes to communicate to each other by sending messages to one another.
  \item Elasticity
The master node has a special role. The master is responsible for checking the 
source code repositories periodically and dispatching a build job for each of 
these repositories to one of the slave nodes in the network. If the queue grows 
in size, the master should provision and bootstrap more virtual machines in 
order to reduce the queue size. If slave nodes have not received a command 
to build software from a specified source code repository for a few minutes 
they automatically shut their own virtual machine down, thus releasing that 
resource and terminating themselves. This way the cluster can quickly grow 
in size during periods of peak loads, whilst reducing itself to the bare minumum 
of one node during idle times.
  \item Performance
Booting and bootstrapping a new VM can take two to three minutes. A build job on 
one of the slave nodes can take anywhere from a minute to several hours. So 
we do not want to be starting and stopping these virtual machines all the time. 
But what we also do not want, is that our system never releases its resources, 
because this would cost a lot of money. So we need to find a good balance, 
between when to provision new virtual machines and when to shut them down 
again. More on this topic in subsection \ref{sec:policies}.
  \item Reliability
As we already stated in the previous item "Elasticity", the master is able to 
automatically scale the cluster according to the queue of build jobs. 
But what if the master crashes, or fails for 
whatever reason? The rest of the cluster would remain idle without a master and 
eventually terminate because the cluster was idle. This is obviously not something 
we want. Each node is notified by JGroups when a node enters or leaves the 
cluster. Usually this is not important for slave nodes. However if one slave node 
notices that the master is missing from the cluster that node is allowed to issue 
an election. When an election is started, a ring is formed between the remaining 
nodes based on their IP address. Then each node sends a message to its neighbor 
with the proposed master of his choice. The other nodes in the network pass 
these messages to their neighbors until the message reaches the original sender. 
Each node is allowed to change the proposed master in the message to himself 
as long as his IP address is `higher' than the previously proposed master. This way 
when each node receives his original message again, that node now knows which 
node has been elected master throughout the cluster. This node will drop the role 
of slave node and assume the role of master node. This means that this node will 
no longer perform source code builds, but will monitor and manage the cluster 
health instead. This node will also no longer terminate after a few minutes of 
inactivity.
  \item Monitoring
The JGroups library automatically notifies us when a node is entering the cluster or
has been dropped from the cluster. This way the master always know which nodes are up and 
which are booting or have stopped working for some reason. But we also wanted 
to be able to monitor the cluster ourselves during experiments so we also tasked 
the master node with monitoring the cluster and writing this information to a special file on the disk 
periodically. This file is also copied to the DAS-4 file server where we can watch 
changes using linx commands like `watch' and `tail' to view the state of the 
cluster in real time in the command line. This is especially useful since we do not always know
which node in the cluster has been elected for the master role.

\end{itemize}

\subsection{System Policies}
\label{sec:policies}
%Describe the policies your system uses and supports. The latter may remain not
%implemented throughout your coursework, as long as you can explain how they can
%be supported in the future.

As explained in the item `Elasticity' in the previous subsection, we use a very 
simple policy to scale our cluster according to the work load. We periodically 
poll the size of the queue of build jobs, and try to maintain a virtual machine 
for each item in the queue. Once a virtual machine is booted and bootstrapped 
that machine will take jobs from this queue until it is fully drained. Once the 
queue is drained and no new jobs are pushed to it by the master node, the 
slave node will automatically terminate after a few minutes of inactivity.

This policy is obviously a little too naive. For instance, what would happen if 
we schedule a few thousand jobs at once? The master would proceed to boot 
and bootstrap several thousand virtual machines. At the very least we should 
limit the amount of concurrently running machines to avoid this problem. There 
are more complex policies that we could use. For instance, we could monitor 
the job duration and attempt to schedule future jobs better to avoid booting 
too many virtual machines at the same time.

\subsection{Additional System Features}
%Describe each additional feature of your system, one sub-section per feature.

\begin{itemize}
  \item Scheduling
Currently, there is a very basic job scheduling system. The master periodically 
checks the queue for jobs. If a job has to be dispatched to a slave node, and one 
or more slave nodes are available, it is dispatched to one of these nodes. If no 
slave nodes are available the master waits until a slave node becomes available. 
As described in the "Elasticity" item of subsection \ref{sec:architecture}, a 
different process in the master node might decide to grow the cluster in size by 
booting and bootstrapping additional virtual machines. The scheduling system 
will then have more resources at its disposal to work through the queue of 
build jobs.
  \item Multitenancy
At the moment the system does not fully support multitenancy, but it should not be too hard to build 
multitenancy into our system. Each user could for instance submit one or more 
of his/her source code repositories which will be periodically checked. However, 
as explained in the previous item, we do not yet have a scheduling system 
capable enough to create `fair' schedules amongst different users.
  \item User profiling
As mentioned before, we could monitor the users and their jobs to help when 
scheduling the build jobs. We could also predict future load so that the master can 
ensure that there is enough computing capacity available to deal with future growth of the queue of build jobs.
  \item Durability
At startup the master loads the queue of build jobs from the file server. 
Additionally the master writes the current queue to the file server after every 
change: submitting or processing a build job. This is done to ensure that a 
new master can continue where the old master left off after a crash. We could 
also preserve build results in a similar manner, but this was not deemed important for this prototype.
  \item Benchmarking and Security
Benchmarking and security were not considered.
\end{itemize}

\section{Experimental Results}
\label{sec:results}
This subsection describes several qualitative experiments. These experiments are
reproducible, and give some insight in the basic functionality of the application.
However, they are not as rigorous a proof of the features of the system as an
extensive quantitative experimental evaluation could be. However, within the
time constraints of this assignment, this is the best that could be done.

\subsection{Experimental setup}
%Describe the working environments (DAS, Amazon EC2, etc.), the general workload
%and monitoring tools and libraries, other tools and libraries you have used to
%implement and deploy your system, other tools and libraries used to conduct your
%experiments.

As the experimental application is implemented specifically to run on the DAS-4
cluster, the experiments are also run on DAS-4. Which build job specifically would
be run during the experiments was not very interesting, so we have chosen one of
our own production-quality projects on GitHub: java-gitolite-manager
\footnote{https://github.com/michaeldejong/Java-Gitolite-Manager - API for
managing Gitolite from Java code}. We have done the monitoring of experiments
through the use of extensive logging throughout our application, which we then
monitored `from the outside', through standard Unix shell utilities such as
watch and tail.

\subsubsection{Technical details}
All experiments were run on the DAS-4 cluster. For each experiment, we booted
a single virtual machine. Then we copied the configuration file and Java jar file to 
the virtual machine. Additionally we also disabled the built-in firewall in CentOS 
to allow JGroups to communicate using several ports.
We then proceeded to run the jar file on the virtual machine which, once booted will
act as the master node, and start booting additional slave nodes.

\subsection{Experiments}
In the following paragraphs, we have described the experiments that we have executed.

\subsubsection{Automation and elasticity}
We have done qualitative experimentation with the automation and elasticity features of our proposed
system. We started the platform on a VM on the DAS-4. Then we fed it slowly with new build jobs.
Each of the jobs would take longer then the period between the queuing of new
jobs. That meant, that each time a job was queued, all the slaves would still be
working on a job. The queue would thus contain the new job for some time. The platform
would then start an additional VM and run a slave node on it. After some
time, we would have several VMs running build jobs. Because at some point
the first inititalized VMs would finish their build job, they would start to
pick up newly queued build jobs. The platform would not start new (slave) VMs
at this point; instead, at some point, VMs would start to suspend themselves,
because the queue would remain empty for a prolonged period of time.
We consider this a proper qualitative experiment for the demonstration of the
automation and elasticity features.

\subsubsection{Reliability - master election}
%Describe an experiment in which the master is killed. What happens?
An important aspect of the peer-to-peer design of the application system is the
fact that it can be much more reliable then an application system with a
client-server model. It was important to test this aspect of the system. A very
simple approach was to simply start the platform, enqueue several jobs, which
would lead to the spawning of several slaves, then kill the master - and see
what happens. And of course, this experiment was repeated several times, including
sessions where not just the master, but also one or more slaves was killed. In
each of these use cases, the platform would resume normal operation in a matter of seconds.

\subsubsection{Reliability - slave failure}
%Describe an experiment in which the slave is killed. What happens?
In case a slave node dissappeared from the cluster, the JGroups library notified the system 
and update the view of the cluster. The master then recieved the updated view and 
notice that a slave node was missing from the cluster. The master would not take any
immediate actions to counter this, but it would periodically check the number of 
active slave nodes against the size of the build job queue. If the master deemed the 
cluster to be too small to clear the queue, the master would proceed to provision 
additional slave nodes.

Now there are two possible states in which the slave might be when it terminates: 
It was idling, or it was building a source code repository. In the first case it is most 
likely because the slave node was idle for a matter of minutes and decided it should 
terminate to release its resources back to the cluster. In the latter case it is most 
likely that the slave node crashed or encountered an exception. In this case the 
slave node dissappears from the JGroups view as described in the previous paragraph, 
and the job it was executing will remain in the building phase. Although this is 
not perfect, it is of little consequence to the rest of the (active) cluster. In the future 
the master should also attempt to reschedule the crashed job.


\section{Discussion}
\label{sec:discussion}

\subsection{Findings}
Although we had a bit of trouble creating a framework to communicate with 
OpenNebula, the system appears to be relatively stable. In hindsight 
OpenNebula was perhaps not our best option. Amazon's EC2 has a Java API
readily available which would have saved us some time.

Our biggest concern however is testing the code. Since there is no real way of 
simulating the OpenNebula cluster on our laptops, in most cases we were 
forced to compile our code, package it, and transfer it to the DAS-4 cluster 
where we would need to manually provision a virtual machine, and bootstrap 
it, before we could run our code. This very long feedback loop reduces the 
development speed a lot, which means that all the features we need to build 
take a lot longer to build. Also how do we test the provisioning of virtual 
machines and bootstrapping them without running it on the DAS-4 cluster? 
We both like to work in a Test Driven Development fashion, but a system 
like this makes that practically impossible.

\subsection{Extrapolation to huge amounts of users}
It is not very useful to extrapolate the costs of the system in a direct method
to much, much larger amounts of users (i.e. 10, 100, 1000 times more) because of
several reasons.
\begin{enumerate}
  \item The experimental application is designed as a proof-of-concept for the
  current level of service that WantCloud currently provides as its business.
  Some future growth is taken in to account as part of the design requirements.
  However, a 10, 100 or 1000 times larger business will warrant an entirely different
  technical design, for example most probably without an intermediate IaaS
  provider.
  \item It would be expected that usage of really large amounts of services
  would allow for (large) discounts at the IaaS provider. This is not taken into
  account when just extrapolating the costs linearly.
  \item Looking purely at the operational costs made for the cloud provider does
  not cover the total cost of the entire business model. For instance, a large
  support organization is required to service such a large amount of customers.
  When organizations grow larger, such costs will not linearly scale with them.
  It is therefore pointless to just focus on the costs of IaaS usage.
\end{enumerate}

\section{Conclusion}
\label{sec:conclusion}

The constructed system leverages Cloud Computing concepts to provide a 
scalable and reliable build infrastructure for software developers. Although still 
very much a prototype it does a very good job of showcasing the advantages 
of such a system. We have shown that this system scales with the load and 
will thus always provide the required computing capacity. This ensures that 
software developers will be notified of failing builds quickly, even at peak load times, 
which in turn is good for the quality of the products in development.

\appendix

\section{Time sheets}
\label{sec:time-sheets}
With a two-person team, we have spent 150 hours total on the work for this assignment.
This can be broken down into the following parts:
\begin{itemize}
  \item Thinking time: 20 hours.\\
  From brainstorming for ideas to discussions on which distributed algorithms to use.
  \item Development time: 70 hours.\\
  Total time spent developing the experimental platform.
  \item Experimentation time: 10 hours.\\
  We have conducted several experiments, albeit each of them being rather small,
  and we only did qualitative experimentation. Note that development time is not
  included in this number, as we did not write any code that was only useful for
  a single experiment.
  \item Analysis time: 10 hours.
  \item Write time: 20 hours.
  \item Wasted time: 20 hours.\\
  The term waste suggests that the time spent did not support the results of the
  assignment. This is true for our initial attempts to get OpenNebula on DAS-4
  to work (e.g. the FAQ was only published after we got OpenNebula working).
  However, it can be argued that we did learn a lot from these failures,
  and therefore waste is a bit of a strong term here.
\end{itemize}

\end{document}
